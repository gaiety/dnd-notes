We take a short rest.
We teleport South to the Lake of Madness then head East. We spot newly posted guards and launch a surprise attack on them.
Otter, who is now a giant scorpion, takes out the two orcs easily.
We head North and slay all the new people with fireballs and questionable Kai tactics.

Monument with power, red wizards praying and fixed on the monument. The momument has vertical tiers.
A thayan with a mop. Other wizards, including a higher level wizard.
Kai had to run back for his equipment.
Each room has cots and personal posessions.

Discover lore: 
(Metminway) Metre Minuit is making sure that his plan will work by channeling the souls of the Chosen into the phylacteries of his underlings, which are stored in the Phylactery Vault beneath the Doomvault.

Two scrolls of detect magic, two scrolls of identify, two scrolls of remove curse,
an instructional scroll by "Lahnis" describing a strange ritual to turn soulbound undead into non-undead creatures.
Tome with tongues pinned to the cover.
