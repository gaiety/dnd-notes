# 01/15/2018

The Defenders of Alkai Tor head back the portal to the dungeon they left from. We are back in the room with the dias where the orcarina was. We see dead gunther (reported missing 2 and a half weeks ago). We see a bronze dragonborn named Orca.

I hear a ghostly whisper of Kuzanbo saying "God damnit"

Our horses are dead from most likely wolves. Our cart is there.

We meet an old man who tracked down Baren Gunther Grimm. He transforms into a hobgoblin dressed all in black.

Cralek attacks us with orc minions. He steals Gunther's body and teleports away to retrieve the bounty before Orca can claim it.

Daisy has already paid for the bounty to Cralek.

We're in the town of Duraleim, we go to the Vermilion Minotaur (inn). We're remembering Baren Habb and ManRat.

We seek an audience with Baren Hab. We get inside a very well protected and the inside seems bigger than the outside. I saw a painting of a face that matched something on a coin I had.

Gunther had lost his membership from the adventureland executive lounge. Rather falling from grace. Blueridge mountains are incredibly difficult to climb (beasts, weather) and maybe teleportation where you cant seem to escape.

Others were disappearing - Reavers (Slavers) have been picking off travelers who don't take the spirit way. Baren Hab's sources have tracked them to a cave. We're staying at the Vermillion Minotaur and we'll be picked up in the morning and given four horses.
