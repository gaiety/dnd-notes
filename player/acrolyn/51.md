Almost completed a lvl 1 keep ($4k short to complete)
In your retinue already:
1 unit of green light archers
1 artisan follower - The Captain (captain price)
1 artisan follower - Farmer (Ole man marshtofarm)
1 special Ally: a Young White Dragon (Nostatorem) (who is chaotic good)

Revelenze gave us a letter with a seal on it. “Kaim has the ability to sense the undead. They are moving in unison now. Remember who to trust.”

“Leader of the Kashife, seeking to end the undead plague.” They have interest in our participation in the tournament. They’ll assign us a task.

They will send an agent to meet with us at the tournament.

Revelenze buys our flame tongue dragon staff for the focus, he has a meeting with Draconov (elder dragons, can shapeshifter) and it’ll put him in good standing.

Grundy fetches us some infernal whine.

Revelenze speaks with us privately in our new war room. The shadow fey have mentioned Mitmenwey by name. They were reporting to him via the portal in the Fey. Revelenze’s tower was at an ideal place, a magical strongpoint which is why they were trying to establish a portal there. 

We throw a huge party, and invite Lendril Pendragon and Jamethy.

Skippy the pirate and parrot, we don’t know if it’s the pirate or the bird who can’t talk or if Skippy is the bird or pirate’s name… LOL

Lucky’s name is Juranimals Brooke. Lucky’s mom is Genjamin, dad’s name is Stephaniel.

Two separate half to the tournament, a team component and an individual component.

Cralek is my old rival.

We get wrist bands and Purple team color pendants (labeled 1, 2, 3). We have a team tent!

Roughly a thousand competitors.

There’s a pill in an envelope saying “Take for good luck”.

—

“In parade, stay in loose formation in line. Do not deviate from the parade route. Follow those in front of you. No funny business. No fighting of any kind, including attack magic. Pay respect to the king by bowing. Sanctum salute (bang fist or shields) to the Farangian Guard).”

The king is here, there are Giants, there’s the Draconov coalition.

Gruesome Flameguard the fire giant is here, it’s Sarath’s enemy. (Velikan Federation lead by Styrke Thundermite)

The Baron is there.

There’s a side group, Caladus the master of thieves is there.

Queen Belator Regina
King Tomothy the Wise

Every Piece a Pawn, Every Pawn a Piece.

Those who show valor can be given higher positions in the expansion army. 

We’re handed a stone and we are directed to a portal. The stone will bear a marking of a sword or shield. It must be visible and hanging from a team member. We must capture at least one stone of each, then get to the center of the zone to complete the round. Excessive force will not be tolerated.

We find ourselves in a dark cave. Our stone is a Shield, we give it to Lucky.


