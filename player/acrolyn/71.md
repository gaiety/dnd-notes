In the far realm cysts the creatures of chaos keep intruders away from a powerful shrine.

Blood-red Circle, twisted runes with a purple light.

Pools and piles of rotting garbage. Unbearable stench. Four stalagmites. Con saves for the smell.
A grotesque creature on 3 legs emerging from the trash, bloated body with tentacles and spikes and eye stalks.
More babies come out! They can grapple us.

We try doing mounted combat as I shoot my bow from the Elk's back. Alas my character is bad at hitting them with a bow haha. They have poison bites too.

Rough walled cavern. Shadows whirling around a gate, contact stone. Many unarmed skeletons. Arty channels divinity and clears out nearly friggen every skeleton...
We use the contact stone and atune our keys.
We activate a black gate.

Kai finds himself unable to use black gates... he grumps off.
The party goes back to try and use the long rest pools. They are reduced to half their maximum hitpoints.

We go back to the entry room to this area and take a long rest. Afterwards, Cleric uses 3 Remove Curses.

We go to the Caverns of Chaos. Stalagmites, and multiple levels. This likely refers to the creatures of chaos keeping intruders away from a powerful shrine!
We launch into a surprise round.
Arty does Protection from Good and Evil on me for 10 min AND a motivational speech.

Syranna tells me privately: "If I teleport out of the vault I'm going to die. She suspected there was something different about me, but realized my body was separated from my soul. Not voluntary. I wouldn't notice any change. Over the course of some weeks I'll lose my identity and may even wight. If I can get to the undying laboratory I can return to normal somehow, in the Masters Domain near the Halls of Necromancy."
I CAN teleport... but not outside of the Doomvault or I'll die.
