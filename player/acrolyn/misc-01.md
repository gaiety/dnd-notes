Sing a bardic song to Lucky about how the shield must be protected from continued abuse of magic. (Kai doesn’t want the wizard to win, he gathered this adventuring party to one-day end the undead plague by finding the best of the best)
(Bardic inspiration works for 10 minutes)

Let us learn from mistakes
To do what it takes
To never again fall
To be above Magic’s corrupt call

People of the shield can no longer rest
Because magic thrusted us into this mess
For a fresh start
We need leaders with strong heart

