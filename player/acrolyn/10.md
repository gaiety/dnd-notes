# 06/05/2017

Andddd we're in the basement. Goodbye Mara! Hello Singer the Bird Bard!

Singer mimics to us the sound of an ogre, who is The Butcher. We hear of the
name Custos? Prayers and whispers of the legendary one-winged dragon who defends
the swamp from evil. Only when truly terrible things things happen does Custos
appear.

Blood splattered room, even the walls. Piles of gore on the swampy floor.
Massive wooden table. Enormous butcher cleavers are embedded in it. Some barrels
and cloths. The Butcher is within! We are crushing him >:3 my orb power cast
lightning! But in consequence I am now speaking in incoherent babbling. Killed
the shit out of him.

Several hallways, each are full of growths of swamp invading the basement. We
find ourselves outside, warped vegetation, clouds of green yellow gas, cruel
wind, acid rain, cluster of winged atrosities outside. This place is Avernus,
the first layer of the nine hells. Empty battlefields lay out before us, broken
weapons and bodies. Zareal appears as an angel whose eyes burn with a bright
light. FUCK THAT PLACE.

We head to another door which I hear heavy breathing from. I fail my stealth
check on it but we have weapons ready. Horror beyond anything we've seen in
here. Sitting is a pale figure vast and lowsome, clawed hands, fanged maw, looks
fiendish.  Let's kill it! (Likely the daughter of the master).

Upon slaying her we book it out as the whole place begins collapsing. We leave
and the places collapses behind us magically, like the closing of a portal.

We go to Gidean's room and it's been ransacked.

Storm convened with his guide. He's told to test his mettle at Odin's Peak.
Known as a retirement home for members of the King's Piece core.

Revelence did not answer his sending stone, it was not on him - we can tell
because the stone tells us magically.

We set up the manor as a hospitality waystation for traveling people. We hire
servants, protection, and supplies for it.

We travel back to adventureland. There's a knight from the piece core who's
running the dungeon solo. A group has gathered to watch. He's doing incredibly
well. He's at least 7'5" dragonborn blue in full plate known as Imperium. 

Poster for the Piece Tournament in 9 months.

A week later we arrive at Odin's Peak. There's a path heading up the mountain.
Small town at the base. Home to Legend's Keep (hall of fame for members of the
piece core).

In the museum of Legends Keep, thousands of years of heroes. Each hero is a hero
from 1-100+years (like elves). Many killed in battle or used in the expansions.
Assisting in aiding the evacuation of Shamara.
