# 07/17/2017

Brutus's last memory was walking into the town Breakwater Harbor. It'll only take a week and a bit to get there along the spirit way.

On the road to Breakwater Harbor the road has two orcs who try to trade their dead horse for our living one. Bad move, we start kicking their ass. One of them was formerly an orc chief but was kicked out of their tribe.

We pull up to the town sitting on the great lake known as the Howling K. Primary economy is derived from fisheries which trade on the spirit way. The waters is a great giver and taker of life. We're taken to the Whispering Boson. Travelers are the primary clientell and many scribbled messages from happy clients, lots of singing and dancing.

Eli and Brutus used to work together. Eli had greater ambitions, involving bloodshed, but they were great friends so it was difficult when they parted ways.

Some hooded people are blending into the crowd and not showing their faces. Brutus evidently has secret verbal signals with a  waiter. We must speak softly. Even another bard who our Kenku mentions it's best not to rock the boat. This bar has 1 copper beers but wines worth hundreds of gold, strange given the generally poor looking crowd. Rumors of attacks on the island of Tera by half human half bird creatures - they believe Poseidon is punishing them for not worshiping him during fishing. One sailer even ran his ship ashore to escape the creatures. The church just wants people to pay more respect. I do notice some people in the room have wine...

The same waiter does an exchange of parchment for coin. We realize we need to go. An orc and many body guards demand Brutus come with them. I stay behind but the rest of the party goes with. I attempt to stealth after and find alternative ways in but no luck.

I end up having to hide two buildings away in an alley and I act homeless under a blanket.

Boss offered to pay debt of Eli's in exchange for Brutus's services. Many, many layers of security until they reach the boss. Warnings of having Brutus' name whispered. Brutus is told he owes the boss greatly and owes a great service. The boss is Caladus, the king of thieves. Desires a Jade coffer, a suicide mission. We have a week. Brutus gets a black spot.

Wow Brutus is a thief.

Orion Chamber 11 is the bank this Jade coffer is in. Best bet may be to go to Wateeza where some contacts are. He's giving the Robin Hood speel. But he is guarenteed telling the truth, even if it's maybe an excuse to make himself feel better about his livelihood.

Keep is damn near impenetrable. Gonna need all our skills to break into this place. Prior, Brutus worked here. Bank regularly short-changes its workers.

This town is one many folks stop at. Many lords stop here so the economy is decent. Town supports the druidic library.

We try to learn who owns Chamber 11.

Plan: 

* Who owns Chamber #11 - through contacts?
* Learn more about the bank and its current state.
* Library, learn more about the symbol on the jade coffer.
* Ragnar will deposit his hammer in the bank.

Lord Barenhab lives to the NorthEast and it is his symbol on the coffer. Durhaleim was the host for the old king of theives. It was believed the guild was disbanded after that seat of power was taken. A single hero out in the night who was dismanteling their operations (a man who dressed as a rat). Happened 20 years ago. Lord may have made some questionable decisions.

Brutus has found a contact to exchange a map with him.

Rooms are certainly trapped. Only known way in is the front door. Guard at door plays a magic tune on bagpipes to open the frnt door. Guarded by guards, hounds, constructs, gargoyels. Portals teleport in an army of reinforcements.

For every lock that has a key, it can be picked. Two people hold keys, the conceirge and the guard master.

We have a bag of holding which we can use to fake deposit the hammer. We'll put the guard master to sleep, which we can do twice, which buys us two minutes before the guard rotation or him waking up fucks us.

1. Walk in
2. Say we'll make a deposit of the hammer to the dwarf.
3. Put guard master to sleep
4. Take key
5. Take out comfy chairs, automen, and blanket from bag of holding
6. Prop him up in a chair and prep sleep #2
7. Open Chamber 11 with key.
8. Check traps, deal with second layer of security.
9. Replace coffer.
10. Deposit OLD warhammer.
11. New hammer into the bag.
12. Leave and allow Singer to recreate the bagpipe musical intro.
