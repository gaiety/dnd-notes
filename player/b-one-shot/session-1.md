Klotra - Dragonborn (Cleric)
Karl(etus) - Human Fighter (ranged)
Vi Hammerdust - Human Barbarian

We're a band of thieves. We're contracted out to do various deeds. We have some notariety. The Bold Embers, is our name.

Town of Redbark.

We're requested to meet a client at the Blue-eyed Raven Tavern.
Dusk and drinks.

Gentlemen approaches us. Asks if we're part of the Bold Embers.
He opens a secret door in a tavern we were previously unaware of.

We're introduced to an important looking rogueish individual.
Small sack (coinpurse), parchment, and a medalion with a symbol we don't recognize.
Mission is to reclaim some documents from a property that his associates own, and his partner has gone missing but the documents are the priority. Partner was sent to pick up the documents 3 weeks ago and hasn't been seen since.

Small farm out of town, North-East a days journey. Should be abandoned. We're given a map of the property. We're shown a sigil of a leaf, the documents should match the leaf. Should be upstairs in the study.

We get a description of the the partner and we can know it's him by saying we're working for Mr Carver.

We get to the depot and say we're looking for the ride arranged by Mr Carver. We have a cart.

We travel through the Redthorn Forrest. Rather uneventful.

We arrive at dusk.

The back door is busted to heck, the front is too but I manage to mage hand it open. Looks properly abandoned.
We head upstairs. The door on the left appears to have been recently handled.

We enter the left door. There's a hutch. There's a table and desk. This room looks a little cleaner... Vi looks through many papers in the desk, none have the leaf sigil. The papers look scrawled upon. Notes look like they seem like shorthand, pidgeon-script. One paper has a small bit of the leaf - but it looks like it was ripped from something else.

We open the other door, which promptly falls off its hinges to the ground with a loud crash.

Klatra looks through a chest and finds a bound leather tome. It's a manifest from a long while ago with accounts and clients. Looks like there's a missing deed amongst this. These look like part of the records but not everything.


There are three hooded figures out front! They come inside and go to the cellar. We follow down. I investigate a chest, it looks like it was trapped in some way leading to some cord. I sever the trap so it won't trigger anything. Inside is clothes, it's a bunch of misc things that don't seem to match anything we've seen - or like it's multiple people's clothes. The door out of here is also trapped in a similar way that we disarm. I open it.

Inside the next room is... a whole floor of bones. There's a panel recessed in the wall. We find a secret door and it goes into a hallway that descends.

We begin hearing murmurs, chanting. We get ready for a hallway ambush, trying to open the door at a distance by making it have a sound.

We head inside. We see a table with a corpse on it... it's splayed open with its limbs tied and it all of the organs are gone. Other corpses are like this as well. We don't see others, but we certainly hear chanting coming from a sigil in the center of the room. There are seven crypts, they are open.

Vi gets ready to smash a crypt, the chanting gets louder and the door slams behind us. We hear "I was wondering when you were going to come down here." it appears to be Mr Carver without his glimmer. They express a need to feed themselves. They appear to be drow in tattered robes. A seven pointed symbol with a vague leaf shape in its center. They have unusually long teeth and nails.

Combat ensues! Eventually we are victorious as Carver explodes into a light and fire, leaving us covered in soot.

A piece of paper floats from the ceiling, it's the deed that we were looking for!
